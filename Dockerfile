FROM python:3.7.2-alpine3.8

MAINTAINER Philip Bormotov <p.bormotov@brandquad.ru>

RUN apk update
RUN apk add bash \
	gcc \
	build-base \
	linux-headers \
	postgresql-dev \
	nginx && \
	python3 -m ensurepip && \
    pip3 install --upgrade pip && \
    rm -r /root/.cache

RUN pip3 install uwsgi

COPY /src /etc/nginx/sites-available/default

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

RUN apk del g++ build-base
RUN apk del gcc\
    linux-headers\
    build-base

RUN chmod +x run_celery.sh
RUN chmod +x run_django.sh

ENV PYTHONUNBUFFERED=1

#ENV STATIC_ROOT=''
#
#ENV BROKER_URL='amqp://myuser:mypassword@localhost:5672/myvhost'
#
#ENV DB_NAME='webbfb'
#ENV DB_USER='webbfb'
#ENV DB_PASSWORD='123456Aa'
#ENV DB_HOST='127.0.0.1'
#
#ENV PROXY_TOKEN='6ab673138778e4719d1d44e4b1234cf39ed22fa7'
#ENV PROXY_URL='https://bfbproxy.brandquad.ru'
#
#ENV SCRAPY_TOKEN='222'
#ENV SCRAPY_URL='http://0.0.0.0:5858'
#ENV SCRAPY_URL_EXT='https://scrapy.stage.brandquad.ru'
#
#ENV QATEST_TOKEN='222'
#ENV QATEST_URL='http://0.0.0.0:5859'
#
#EXPOSE 8000