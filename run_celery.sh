#!/bin/sh

# wait for RabbitMQ server to start
sleep 5
cd src
celery -A web worker --beat --scheduler django --loglevel=info