#!/bin/sh

# wait for PSQL server to start
sleep 5
cd src
#python manage.py runserver 0.0.0.0:8000
gunicorn web.wsgi:application --bind=0.0.0.0:8000 --workers=20 --timeout=300
#uwsgi --http-socket :8000 --py-autoreload 1 --module webbfbsite.wsgi:application