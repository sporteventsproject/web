from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'web.settings')
os.environ.setdefault('C_FORCE_ROOT', 'true')

BROKER_URL = os.getenv('BROKER_URL', 'amqp://guest:guest@localhost:5672/')

app = Celery('web', broker=BROKER_URL, backend=BROKER_URL, include=['web.main.tasks'])

app.autodiscover_tasks()
