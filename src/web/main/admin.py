from django.contrib import admin
from .models import Event, BetGroup, BetValue, BK


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


@admin.register(BetGroup)
class BetGroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'sub_name')


@admin.register(BetValue)
class BetValueAdmin(admin.ModelAdmin):
    list_display = ('bet_group', 'bk')


@admin.register(BK)
class BKAdmin(admin.ModelAdmin):
    list_display = ('id_odds', 'name')