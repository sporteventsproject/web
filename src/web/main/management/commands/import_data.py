import json

from django.core.management.base import BaseCommand, CommandError
from ... models import Event, BetGroup, BetValue, BK

class Command(BaseCommand):
    help = 'import bk'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        with open('bkrs.json', 'r') as f:
            jo = json.loads(f.read())
        for id, bk in jo.items():
            bk_obj = BK(id_odds=id, name=bk.get('WebName', ''),
                        url=bk.get('Url', '') if bk.get('Url', '')!= None else '')
            bk_obj.save()


        print('import finish')
