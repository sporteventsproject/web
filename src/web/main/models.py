from django.db import models


class BK(models.Model):
    id_odds = models.CharField(max_length=200, unique=True)
    url = models.CharField(max_length=200, default='')
    name = models.CharField(max_length=200, default='')

    def __str__(self):
        return '%s' % self.name


class Event(models.Model):
    id_odds = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200, default='')
    url = models.CharField(max_length=200, default='')
    date_add = models.DateTimeField(auto_now_add=True, null=True)
    data_start = models.DateTimeField(null=True)
    data_end = models.DateTimeField(null=True)
    active = models.BooleanField(default=True)
    group = models.CharField(max_length=200, default='')
    sport_type = models.CharField(max_length=200, default='')

    def __str__(self):
        return '%s' % self.name


class BetGroup(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    id_odds = models.CharField(max_length=200, default='')
    name = models.CharField(max_length=200, default='')
    sub_name = models.CharField(max_length=200, default='')
    url = models.CharField(max_length=1000, default='')
    url_data = models.CharField(max_length=1000, default='')

    def __str__(self):
        return '%s' % self.name


class BetValue(models.Model):
    bet_group = models.ForeignKey(BetGroup, on_delete=models.CASCADE)
    bet_sub_group = models.CharField(max_length=200, default='')
    bk = models.ForeignKey(BK, on_delete=models.CASCADE)
    k_bk = models.FloatField(default=0)
    k_ref = models.FloatField(default=0)
    value = models.FloatField(default=0)
    date_update = models.DateTimeField(null=True)

    class Meta:
        unique_together = ("bet_group", "bet_sub_group", 'bk')

    def __str__(self):
        return '%s' % self.pk
