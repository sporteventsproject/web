from __future__ import absolute_import, unicode_literals
from celery import shared_task
import os
import requests
import json
import time
import traceback
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)




@shared_task(name="test")
def test_task():
    return 'Hello world'