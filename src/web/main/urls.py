from django.contrib import admin
from django.urls import path
from web.main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('account', views.account, name='account'),
    path('events', views.events, name='events'),
    path('betvalue', views.betvalue, name='betvalue'),

    # api
    path('upload-events', views.upload_events, name='upload_events'),
    path('upload-bet-values', views.upload_bet_values, name='upload_bet_values'),

    path('get-bet-groups-urls', views.get_bet_groups_urls, name='get_bet_groups_urls'),
    path('get-bet-data-urls', views.get_bet_data_urls, name='get_bet_data_urls'),

]
