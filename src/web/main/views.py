import requests
import json
import re
import string
from random import choice
from copy import deepcopy
import os
import urllib.parse
from datetime import datetime

from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse, Http404
from django.utils import timezone
from datetime import timedelta
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.core import serializers
from django.conf import settings
from django.db.models import Sum
from django.http import JsonResponse
from django.contrib.auth.forms import User

from .models import Event, BetGroup, BetValue, BK


def index(request):
    template = loader.get_template('main/index.html')
    one_day_ago = timezone.now() - timedelta(days=1)
    context = {
        'completed_jobs': '',
        'running_jobs': '',
    }
    return HttpResponse(template.render(context, request))


def account(request):
    template = loader.get_template('main/account.html')
    bks = BK.objects.all()
    current_user = request.user
    context = {
        'bks': bks,
        'user': current_user,
    }
    return HttpResponse(template.render(context, request))


def events(request):
    template = loader.get_template('main/events.html')
    events = Event.objects.all().order_by('data_start')
    context = {
        'events': events,

    }
    return HttpResponse(template.render(context, request))

def betvalue(request):
    one_day_ago = timezone.now() - timedelta(days=1)
    template = loader.get_template('main/betvalues.html')
    betvalues = BetValue.objects.all().order_by('-value')
    betvalues = betvalues.filter(k_bk__gte=1.6,
                                 k_bk__lte=2.2,
                                 value__gte=4,
                                 bet_group__event__data_start__gte=one_day_ago,
                                 # bk__name__contains = 'marathon'
                                 )

    context = {
        'betvalues': betvalues,

    }
    return HttpResponse(template.render(context, request))


@csrf_exempt
def upload_events(request):
    if request.method == 'POST':
        jo = json.loads(request.body)
        for event in jo:
            event_obj = Event.objects.filter(id_odds=event['id']).first()
            if not event_obj:
                event_obj = Event(id_odds=event['id'],
                                  name=event['name'],
                                  url=event['url'],
                                  data_start=datetime.fromtimestamp(int(event['data_start'])),
                                  group=event['group'],
                                  sport_type=event['sport_type'],
                )
                event_obj.save()

                betgroups = [
                    {'url': '?r=4#1X2;2', 'name': '1X2', 'sub_name': 'Full Time', 'id_odds': '1-2'},
                    {'url': '?r=4#1X2;3', 'name': '1X2', 'sub_name': '1st Half', 'id_odds': '1-3'},
                    # {'url': '?r=4#1X2;4', 'name': '1X2', 'sub_name': '2nd Half', 'id_odds': '1-4'},

                    {'url': '?r=4#ah;2', 'name': 'Asian Handicap', 'sub_name': 'Full Time', 'id_odds': '5-2'},
                    {'url': '?r=4#ah;3', 'name': 'Asian Handicap', 'sub_name': '1st Half', 'id_odds': '5-3'},
                    # {'url': '?r=4#ah;4', 'name': 'Asian Handicap', 'sub_name': '2nd Half', 'id_odds': '5-4'},

                    {'url': '?r=3#over-under;2', 'name': 'Over/Under', 'sub_name': 'Full Time', 'id_odds': '2-2'},
                    {'url': '?r=3#over-under;3', 'name': 'Over/Under', 'sub_name': '1st Half', 'id_odds': '2-3'},
                    # {'url': '?r=3#over-under;4', 'name': 'Over/Under', 'sub_name': '2nd Half', 'id_odds': '2-4'},

                ]

                for gr in betgroups:
                    bet_gr_obj = BetGroup(event=event_obj,
                                          id_odds=gr['id_odds'],
                                          name=gr['name'],
                                          sub_name=gr['sub_name'],
                                          url=event['url'] + gr['url'])
                    bet_gr_obj.save()
        return HttpResponse('Events update successful', content_type='text')
    else:
        return HttpResponse('GET method not allowed', content_type='text')


@csrf_exempt
def upload_bet_values(request):
    if request.method == 'POST':
        jo = json.loads(request.body)
        for bet_gr in jo:
            bet_gr_obj = BetGroup.objects.filter(id_odds=bet_gr['group_id'], event__id_odds=bet_gr['event_id'])
            bet_gr_obj.update(url_data=bet_gr['url_data'])
            bet_gr_obj = bet_gr_obj[0]
            for bet_val in bet_gr['bet_values']:
                bk_obj = BK.objects.get(id_odds=bet_val['bk'])
                BetValue.objects.update_or_create(
                    bet_group=bet_gr_obj,
                    bet_sub_group=bet_val['bet_sub_group'],
                    bk=bk_obj,
                    defaults={'k_bk': bet_val['k_bk'], 'k_ref': bet_val['k_ref'],
                              'value': bet_val['value'], 'date_update': timezone.now()},
                )

        return HttpResponse('Bet values update successful', content_type='text')
    else:
        return HttpResponse('GET method not allowed', content_type='text')


@csrf_exempt
def get_bet_groups_urls(request):
    # bet_groups = BetGroup.objects.filter(url_data='')
    bet_groups = BetGroup.objects.all()
    urls = bet_groups.values_list('url', flat=True).distinct()
    urls = list(urls)
    # return HttpResponse('GET method not allowed', content_type='text')
    return JsonResponse({'urls': urls})


@csrf_exempt
def get_bet_data_urls(request):
    # bet_groups = BetGroup.objects.all()
    bet_groups = list(BetGroup.objects.values('url', 'url_data'))
    return JsonResponse({'data': bet_groups})

