from django.urls import path
from web.telegram import views

urlpatterns = [
    path('getme', views.getme, name='getme'),
    path('getmessage', views.getmessage, name='getmessage'),
]