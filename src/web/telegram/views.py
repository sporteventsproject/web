import requests
import json
import re
import string
from random import choice
from copy import deepcopy
import os
import urllib.parse
from datetime import datetime

from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse, Http404
from django.utils import timezone
from datetime import timedelta
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.core import serializers
from django.conf import settings
from django.db.models import Sum

from web.main.models import Event, BetGroup, BetValue, BK

TELEGRAM_URL = 'https://api.telegram.org/bot525248862:AAFpguMUGbB63tur-YpQQMqB0dmTitX6hgg/'

proxies = {
    'http': 'socks5://suser:123Dsa123@67.205.133.157:1080',
    'https': 'socks5://suser:123Dsa123@67.205.133.157:1080'
}

def getme(request):
    r = requests.get(url=TELEGRAM_URL+'getme', proxies=proxies)
    return HttpResponse(r.text)

@csrf_exempt
def getmessage(request):
    if request.method == 'POST':
        r = request.body
        print(r)
    return HttpResponse('ok')