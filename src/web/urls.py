from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .settings import MEDIA_ROOT, MEDIA_URL

urlpatterns = [
    path('telegram/', include('web.telegram.urls')),
    path('auth/', include('web.loginsys.urls')),
    path('', include('web.main.urls')),
    path('admin/', admin.site.urls),
]

urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()